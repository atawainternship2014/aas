from setuptools import setup, find_packages

# dans install_requires    :   'ipdb', 'ipython', 


setup(
    name='aas_display_handler',
    version='0.1',

    author='Pierre Roux',
    author_email='pierre.roux01@gmail.com',

    packages=find_packages(),
    install_requires=[
        'click',
        'num2words',
        'psutil',
        'pystache',
        'twisted',
    ],

    include_package_data=True,
    package_data={
        'aas_display': [
            'server/conf.d/*',
            'server/screenlayout/data/gpl-3.txt',
        ],
    },

    entry_points='''
        [console_scripts]
        aas_display_server=aas_display.server:run
        aas_display_client=aas_display.client:cli
    ''',
)
