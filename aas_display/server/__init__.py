
import os.path

from .display_server import run

#import ipdb

import logging.config
#logger = logging.getLogger(__name__)
#cmd_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), 'commands'))
#prof_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), 'prof.d'))
conf_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), 'conf.d'))

logging.config.fileConfig(
    os.path.abspath(os.path.join(conf_dir, 'logging.ini')))
logger = logging.getLogger(__name__)

logger.debug('Display Server Loading')
