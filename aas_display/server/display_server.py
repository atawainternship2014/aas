# -*- encoding: utf-8 -*-

from twisted.web import xmlrpc, server

import display_config

import logging
"""
import logging.config
#logger = logging.getLogger(__name__)
#cmd_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), 'commands'))
#prof_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), 'prof.d'))
conf_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), 'conf.d'))

logging.config.fileConfig(
    os.path.abspath(os.path.join(conf_dir, 'logging.ini')))
"""
logger = logging.getLogger(__name__)

APP_NAME = 'aas_display_server'


class CoreHandler(xmlrpc.XMLRPC):
    pass


class DisplayHandler(xmlrpc.XMLRPC):

    def xmlrpc_get_options_auto(self):
        logger.info('Getting options for automatic configuration')
        dc = display_config.AtawaXConfig(auto=True)
        return dc.get_options()

    def xmlrpc_get_options_manual(self):
        logger.info('Getting options for manual configuration')
        dc = display_config.AtawaXConfig(auto=False)
        return dc.get_options()

    def xmlrpc_config_auto(self, product_concept, hardware,
                           nb_screens, multi_mode):
        logger.info('Starting automatic configuration')
        self.dc = display_config.AtawaXConfig(auto=True)

        touch_cap = True
        self.dc.start_config_auto(
            product_concept,
            hardware,
            nb_screens,
            multi_mode,
            touch_cap)
        self.dc.store_config()
        self.dc.apply_config()

        # return dc.show_config()
        return 'config auto: ok'

    def xmlrpc_config_manual(self):
        logger.info('Starting manual configuration')
        self.dc = display_config.AtawaXConfig(auto=False)

        self.dc.start_config_manual()
        self.dc.store_config()
        self.dc.apply_config()

        # return dc.show_config()
        return 'config manual : ok'

    def xmlrpc_get_config(self):
        logger.info('Getting configuration')
        dc = display_config.AtawaXConfig()

        try:
            dc.load_config()
            cfg = dc.show_config()
        except:
            cfg = 'no display config'

        return cfg

    def xmlrpc_validate_config(self):
        logger.info('Validating configuration')
        # dc = display_config.AtawaXConfig()

        self.dc.validate_config()

        return 'validated'

    def xmlrpc_cancel_config(self):
        logger.info('Canceling configuration')
        # dc = display_config.AtawaXConfig()

        self.dc.validate_config()

        return 'canceled'

    def xmlrpc_reset_config(self):
        logger.info('Resetting configuration')
        dc = display_config.AtawaXConfig()

        dc.reset_config()

        return 'reset'

    def xmlrpc_quit_config(self):
        logger.info('Quitting configuration')
        dc = display_config.AtawaXConfig()

        dc.quit_config()

        return 'quit'

    def xmlrpc_touchscreen_attach_output(self):
        logger.info('Touchscreen: attach touchscreen')
        dc = display_config.AtawaXConfig()

        ret = dc.touchscreen_attach_output()

        return ret

    def xmlrpc_touchscreen_toggle_direction_axis_x(self):
        logger.info('Touchscreen: toggle direction axis x')
        dc = display_config.AtawaXConfig()

        ret = dc.touchscreen_toggle_direction_x()

        return ret


    def xmlrpc_touchscreen_toggle_direction_axis_y(self):
        logger.info('Touchscreen: toggle direction axis y')
        dc = display_config.AtawaXConfig()

        ret = dc.touchscreen_toggle_direction_y()

        return ret

    def xmlrpc_touchscreen_toggle_swap_axes_xy(self):
        logger.info('Touchscreen: toggle swap axes x/y')
        dc = display_config.AtawaXConfig()

        ret = dc.touchscreen_toggle_swap_axes_xy()

        return ret

    def xmlrpc_touchscreen_get_config(self):
        logger.info('Touchscreen: toggle swap axes x/y')
        dc = display_config.AtawaXConfig()

        cfg = dc.touchscreen_get_config()

        return cfg

    def xmlrpc_touchscreen_calibrate(self):
        logger.info('Touchscreen: calibrate')
        dc = display_config.AtawaXConfig()

        ret = dc.touchscreen_calibrate()

        return ret

def run():
    PORT = 7080
    from twisted.internet import reactor
    core_handler = CoreHandler()
    display_handler = DisplayHandler()
    core_handler.putSubHandler('display', display_handler)
    reactor.listenTCP(PORT, server.Site(core_handler))
    print "== Atawa Session Handler =="
    print "Listening on {}:{}".format('localhost', PORT)
    reactor.run()

if __name__ == '__main__':
    run()
