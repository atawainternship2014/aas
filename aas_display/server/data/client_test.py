from twisted.web.xmlrpc import Proxy
from twisted.internet import reactor

def printValue(value):
    print value
    reactor.stop()

def printError(error):
    print 'error', error
    reactor.stop()

proxy = Proxy('http://127.0.0.1:7080/XMLRPC')
#proxy.callRemote('start_config_auto').addCallbacks(printValue, printError)
proxy.callRemote(
    'display.start_config_auto',
    'ecranH',
    'nec_a2l',
    2,
    'clone'
).addCallbacks(printValue, printError)
reactor.run()
