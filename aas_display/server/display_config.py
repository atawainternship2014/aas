# -*- encoding: utf-8 -*-

import os
import os.path
import shutil
import time
import subprocess
import re
import json
import tempfile

from datetime import datetime
from threading import Timer
from ConfigParser import RawConfigParser
import pystache
from num2words import num2words

from screenlayout.auxiliary import Geometry, Rotation
from screenlayout.xrandr import XRandR
XRandRConfiguration = XRandR.Configuration
OutputConfiguration = XRandR.Configuration.OutputConfiguration
from screenlayout.widget import ARandRWidget

import logging
#import logging.config
#conf_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), 'conf.d'))
#logging.config.fileConfig(
#    os.path.abspath(os.path.join(conf_dir, 'logging.ini')))
logger = logging.getLogger(__name__)

APP_NAME = 'aas_display_server'

ATAWA_BASEDIR = '/opt/atawa'

SCREENLAYOUT_MANUAL = os.path.join(ATAWA_BASEDIR, 'conf/X11/screenlayout_manual.sh')

# David test des trucs, et commente la ligne du dessous
#import ipdb


class InputConfigError(AssertionError):
    pass


class VirtualLayoutError(Exception):
    pass

class BadStateXsessionError(Exception):
    pass

"""
class PackageRules:

    _rules = []

    class LineRule:
        def __contains__(a, b):
            return a in b

        def __is(a, b):
            return a == b

    def add_rule(self):
        pass

    def check_rules(self):
        return

pkr = PackageRules()
pkr.add_rule()
"""

# Useless ...
#class DisplayConfiguration(XRandRConfiguration):
class MonitorConfiguration(object):

    def __init__(self, num, output_name, output_conf):
        self.num = num
        self.output = output_conf
        self.output_name = output_name
        #self.output_name = str(self.output.size)
        #self.primary = primary


class AtawaXConfig(object):

    # Rules for the config input
    _product_concept = (
        'ecranH',
        'ecranH_FullHD',
        'ecranV',
        'borne',
    )
    _nb_screens = (1, 2)
    _multi_mode = (
        'normal',
        'clone',
        'extend',
    )
    _touch_cap = (True, False)

    profile = None

    # ARandR config
    displays_config = None

    # Config cancelation timer
    config_schedcancel = None

    # Xorg Config
    xorgconf_data = None

    def __init__(self, auto=True):

        self.auto = auto

        self.server_basedir = os.path.dirname(os.path.realpath(__file__))

        self.concepts_conf = RawConfigParser()
        self.concepts_conf.read(os.path.join(self.server_basedir,
                                             'conf.d/concepts.conf'))
        self.hardware_conf = RawConfigParser()
        self.hardware_conf.read(os.path.join(self.server_basedir,
                                             'conf.d/hardware.conf'))

        logger.info('Started Display Congiguration Handler')

        """
        config_input = json.loads(json_input)

        try:
            self.profile = {
                'product_concept': config_input['product_concept'],
                'nb_screens': config_input['nb_screens'],
                'multi_mode': config_input['multi_mode'],
                'touch_cap': config_input['touch_cap'],
            }
        except KeyError, e:
            msg = 'Clé de configuration manquante : {}'.format(e)
            logger.error(msg)
            raise InputConfigError(msg)
        """

    ###################### Get options ############################

    def get_options(self):
        if not self.auto:
            options = {
                #'touch_cap': self._touch_cap
            }
        else:
            options = {
                'product_concept': self.concepts_conf.sections(),
                'hardware': self.hardware_conf.sections(),
                'multi_mode': self._multi_mode,
                #'touch_cap': self._touch_cap
            }
        logger.info('Options for {} configuration: {}'.format('automatic' if self.auto else 'manual',
                                                              options))
        return options

    ######################### Generating conf #########################

    def start_config(self, *args):
        if self.auto:
            self.start_config_auto(args)
        else:
            self.start_config_manual()

    def start_config_auto(self,
                     product_concept,
                     hardware,
                     nb_screens,
                     multi_mode,
                     touch_cap):
        self.profile = {
            'product_concept': str(product_concept),
            'hardware': str(hardware),
            'nb_screens': int(nb_screens),
            'multi_mode': str(multi_mode),
            'touch_cap': touch_cap,
        }

        try:
            self._validate_input_config()
        except AssertionError as e:
            print "lala"
            msg = 'Clé de configuration invalide : {}'.format(e)
            logger.error(msg)
            raise InputConfigError(msg)

        self._generate_config()

        # DONE:
        #   - génération de la configuration vers xorgconf_data
        #   - sérialisation de la configuration

    def _validate_input_config(self):
        assert (self.profile['product_concept'] in self._product_concept), \
            "Clé non paramétrée de concept de produit : {}".format(self.profile['product_concept'])
        assert (isinstance(self.profile['hardware'], str)), \
            "Clé non paramétrée de type de matériel : {}".format(self.profile['hardware'])
        assert (self.profile['nb_screens'] in self._nb_screens), \
            "Clé non paramétrée de nombre d'écrans : {}".format(self.profile['nb_screens'])
        assert (self.profile['multi_mode'] in self._multi_mode), \
            "Clé non paramétrée de mode d'affichage : {}".format(self.profile['multi_mode'])
        assert (self.profile['touch_cap'] in self._touch_cap), \
            "Clé non paramétré de gestion tactile : {}".format(self.profile['touch_cap'])

        # Vérification des règles des concepts
        concept = self.profile['product_concept']
        assert (concept in self.concepts_conf.sections()), \
            "Concept de produit non reconnu : {}".format(self.profile['product_concept'])
        assert (self.profile['hardware'] in self.hardware_conf.sections()), \
            "Type de matériel non reconnu : {}".format(self.profile['hardware'])
        #assert(0 < self.profile['nb_screens'] <= int(self.concepts_conf.get(
        #    concept, 'max_screens')), "Nombre invalide d'écrans à activer")
        assert (0 < self.profile['nb_screens'] <= int(self.concepts_conf.get(
            concept, 'max_screens'))), \
            "Nombre invalide d'écrans à activer : {}".format(self.profile['nb_screens'])
        assert (self.profile['multi_mode'] in self.concepts_conf.get(
            concept, 'modes').split(',')), \
            "Mode d'affichage incompatible avec ce produit : {}".format(self.profile['multi_mode'])

        ##
        # WARNING : règles codées :
        ##

        # Si c'est une borne, l'écran est unique
        if self.profile['product_concept'] == 'borne':
            assert self.profile['nb_screens'] == 1, "Le concept 'borne' n'a qu'un seul écran."

        # Si on a plusieurs écrans, ils sont :
        #   - soit en clone
        #   - soit en étendu
        if self.profile['nb_screens'] > 1:
            assert (self.profile['multi_mode'] in self._multi_mode[1:]), \
                "Mode d'affichage non compatible avec le nombre d'écrans"

    def start_config_manual(self):

        self._generate_config()

        # DONE:
        #   - génération de la configuration vers xorgconf_data
        #   - sérialisation de la configuration

    def _launch_arandr(self):
        with open(SCREENLAYOUT_MANUAL, 'w') as f:
            f.write('xrandr command')

        os.chmod(SCREENLAYOUT_MANUAL, 0666)
        subprocess.check_call(
            'su -c "DISPLAY=:0.0 {}" atawa'.format(
                os.path.join(ATAWA_BASEDIR, 'bin/arandr')),
            shell=True)
        # Cette version modifiée de ARandR écrit dans le fichier
        # défini par la variable SCREENLAYOUT_MANUAL
        os.chmod(SCREENLAYOUT_MANUAL, 0664)

    def _generate_config(self):
        """
        try:
            os.remove('/etc/X11/xorg.conf')
        except OSError:
            pass
        #self._restart_atawa_xsession()
        """

        self._restart_full_atawa_xsession('waitconfig', clear_xorg=True)

        self._load_randr_config()

        logger.debug('current XRandR configuration : {}'.format(self.displays_config._xrandr.configuration))

        if self.auto:
            self._generate_config_auto()
        else:
            self._generate_config_manual()

        self.xorgconf_data['touchscreen']['touchscreen_id'] = self._detect_touchscreen()

    def _load_randr_config(self):
        xlock_wait_time = 0
        while not os.path.exists('/tmp/.X0-lock'):
            if xlock_wait_time > 15.00:
                raise AssertionError('Xorg not started: no lock available')
            time.sleep(1.0)
            xlock_wait_time += 1.0
        time.sleep(10.0)
        self.displays_config = ARandRWidget(display=':0.0')
        self.displays_config.load_from_x()

    def _generate_config_auto(self):
        available_outputs = self.__detect_hardware()
        display_config = self.__generate_displays_data_auto(available_outputs)
        self.__generate_template_data(display_config)

    def _generate_config_manual(self):
        self._create_xorg_resolutions()
        self._restart_full_atawa_xsession('waitconfig', clear_xorg=True, wait=5)
        self._load_randr_config()
        self._launch_arandr()
        self._delete_xorg_resolutions()
        display_config = self.__generate_displays_data_manual()
        self.__generate_template_data(display_config)

    waitconfig_resolutions_file = '/usr/share/X11/xorg.conf.d/60-resolutions.conf'

    def _create_xorg_resolutions(self):
        resolutions_data = {
            'monitors': [],
        }

        for output_name in self.displays_config._xrandr.configuration.outputs.keys():
            resolutions_data['monitors'].append({'monitor_output': output_name})

        resolutions_tpl_path = os.path.join(self.server_basedir,
                                         'conf.d/resolutions.conf.mustache')
        with open(resolutions_tpl_path, 'r') as f:
            resolutions_tpl_file = f.read().decode('utf-8')
        resolutions_tpl_parsed = pystache.parse(resolutions_tpl_file)
        resolutions_rendered = pystache.render(resolutions_tpl_parsed,
                                            resolutions_data)

        with open(self.waitconfig_resolutions_file, 'w') as f:
            f.write(resolutions_rendered)

    def _delete_xorg_resolutions(self):
        if os.path.exists(self.waitconfig_resolutions_file):
            os.remove(self.waitconfig_resolutions_file)

    def __detect_hardware(self):
        # outputs : le premier sera l'écran principal avec le touchscreen
        #   ex: ['LVDS1', 'DVI1']
        # touchcap

        #hardware_target = 'nexcom'  # !!

        hardware_target = self.profile['hardware']

        # Récupération des outputs en fonction du type de la carte
        if hardware_target not in self.hardware_conf.sections():
            hardware_target = 'default'
            logger.error('Hardware card fallback to default')
            raise InputConfigError('Hardware card not found')
        outputs = self.hardware_conf.get(hardware_target, 'outputs').split(',')
        print 'possible_outputs: {}'.format(outputs)
        assert (len(set(outputs)) == len(outputs)), \
            "Les sorties configurées sont invalides : {}".format(outputs)
        logger.debug('Declared outputs : {}'.format(outputs))

        # Filtrage des outputs activées
        available_outputs = []
        accessible_outputs = self.displays_config._xrandr.configuration.outputs
        for output_name in outputs:
            if output_name in accessible_outputs.keys():
                if self.displays_config._xrandr.state.outputs[output_name].connected:
                    available_outputs.append(output_name)
                    logger.debug('output connected : {} ==> kept'.format(output_name))
                    print 'connected', output_name
                    if not accessible_outputs[output_name].active:
                        #outputs.remove(output_name)
                        print 'not active', output_name
                        logger.debug('output not active : {} ==> still kept'.format(output_name))
                    else:
                        logger.debug('output not connected : {} ==> removed'.format(output_name))
                        print 'not connected', output_name
            else:
                #outputs.remove(output_name)
                print 'not available', output_name
                logger.debug('output not available : {} ==> removed'.format(output_name))


        """
        # Filtrage des outputs activées
        for output_name, output_conf in self.displays_config._xrandr.configuration.outputs.items():
            if output_name in outputs:
                if not output_conf.active:
                    outputs.remove(output_name)
                    logger.info('output not active : {} ==> removed'.format(output_name))
                else:
                    logger.info('output active : {} ==> kept'.format(output_name))
        """

        logger.info('Final available outputs : {}'.format(available_outputs))

        print 'available_outputs : {}'.format(available_outputs)

        return available_outputs

    def __serialize_config(self):
        # Serialize in ini:
        #   - input config
        #   - inferred config
        pass

    def __generate_displays_data_auto(self, available_outputs):
        """
        outputs: liste des outputs par ordre de priorité
        """

        assert (self.profile['nb_screens'] <= len(available_outputs)), \
            "Nombre insuffisant de sorties vidéo disponibles."
        configurable_outputs = available_outputs[:self.profile['nb_screens']]

        display_config = []

        stack_target = self.concepts_conf.get(
            self.profile['product_concept'], 'stack')
        resolution_target = self.concepts_conf.get(
            self.profile['product_concept'], 'resolution')
        geometries = self.__compute_geometries(
            len(configurable_outputs), resolution_target, stack_target)
        rotation_target = self.concepts_conf.get(
            self.profile['product_concept'], 'rotation')

        logger.info('targeted stacking : {}'.format(stack_target))
        logger.info('targeted resolution : {}'.format(resolution_target))
        logger.info('computed geometries : {}'.format(geometries))

        for i, o in enumerate(configurable_outputs):
            output_geo = geometries.next()
            display_config.append(
                MonitorConfiguration(
                    num=i,
                    output_name=o,
                    output_conf=OutputConfiguration(
                        active=True,
                        geometry=output_geo,
                        rotation=Rotation(rotation_target),
                        modename='{}'.format(output_geo.size)
                    )
                    #primary=(i == 0)
                )
            )
        return display_config

    def __generate_displays_data_manual(self):
        display_config = []
        self.displays_config.load_from_file(SCREENLAYOUT_MANUAL)


        def order_outputs_by_pos():

            from math import sqrt
            #def output_dist(output_conf):
            #    x, y = output_conf.left, output_conf.top
            #    return sqrt(x**2 + y**2)

            outputs = {}
            for output_name, output_conf in self.displays_config._xrandr.configuration.outputs.items():
                outputs[output_name] = {
                    'active': output_conf.active,
                    'pos': output_conf.position if output_conf.active else '',
                    'dist': sqrt((output_conf.position.left)**2 + (output_conf.position.top)**2) if output_conf.active else ''
                }
            active_outputs = [(on, oc) for on, oc in outputs.items() if oc['active']]
            notactive_outputs = [(on, oc) for on, oc in outputs.items() if not oc['active']]
            assert(len(active_outputs) + len(notactive_outputs) == len(outputs))
            sorted(active_outputs, key = lambda o : o[1]['dist'])
            sorted(notactive_outputs, key = lambda o : o[0])
            outputs = active_outputs + notactive_outputs
            #for i, o in enumerate(outputs):
            #    o[1]['num'] = i
            return {o[0]: i for i, o in enumerate(outputs)}

        outputs_order = order_outputs_by_pos()

        for output_name, output_conf in self.displays_config._xrandr.configuration.outputs.items():
            if output_conf.active:
                display_config.append(
                    MonitorConfiguration(
                        num=outputs_order[output_name],
                        output_name=output_name,
                        output_conf=OutputConfiguration(
                            active=True,
                            geometry=Geometry(
                                output_conf.mode.width,
                                output_conf.mode.height,
                                output_conf.position.left,
                                output_conf.position.top
                            ),
                            #geometry=output_conf.geometry,
                            rotation=output_conf.rotation,
                            modename='{}x{}'.format(output_conf.mode.width, output_conf.mode.height)
                        )
                        #primary=(i == 0)
                    )
                )

            else:
                # TODO: "Disable True"
                pass

        return display_config

    def __generate_template_data(self, display_config):
        self.xorgconf_data = {
            'active_displays': [],
            'passive_displays': [],
            'touchcap': None,
            'touchscreen': None,
        }

        for d in display_config:
            self.xorgconf_data['active_displays'].append(
                {
                    'screen_num': d.num,
                    'screen_id': 'screen-{}'.format(num2words(d.num)),
                    'monitor_id': '{}-{}'.format(d.num, d.output_name),
                    #'monitor_mode': '{}_60.00'.format(d.output.geometry.size),
                    #'monitor_mode': d.output.mode.name,
                    'monitor_mode': '{}_60.00'.format(
                        d.output.size),
                    'monitor_modeline': self.__get_modeline(
                        d.output.size.width, d.output.size.height),  # or d.output.size.name
                    'monitor_x': d.output.position.left,
                    'monitor_y': d.output.position.top,
                    'monitor_w': d.output.size.width,
                    'monitor_h': d.output.size.height,
                    'monitor_rotation': '{}'.format(d.output.rotation),
                    'monitor_output': d.output_name,                # !!
                    #'monitor_primary': '{}'.format(d.primary),
                    'monitor_primary': '{}'.format(d.num == 0),
                }
            )

        accessible_outputs = self.displays_config._xrandr.configuration.outputs.keys()
        active_outputs = [d.output_name for d in display_config]
        assert all(oname in accessible_outputs for oname in active_outputs)
        passive_outputs = [oname for oname in accessible_outputs
                           if oname not in active_outputs]

        bad_outputs = []
        for output_name in passive_outputs:
            for bad_output_name in ['VIRTUAL']:
                if output_name.startswith(bad_output_name):
                    bad_outputs.append(output_name)
        passive_outputs_to_ignore = [oname for oname in passive_outputs
                        if oname not in bad_outputs]

        for output_name in passive_outputs_to_ignore:
            self.xorgconf_data['passive_displays'].append(
                {
                    'monitor_id': '{}'.format(output_name),
                    'monitor_output': '{}'.format(output_name),
                }
            )

        #self.xorgconf_data['touchscreen_id'] = self.touchscreen_id
        self.xorgconf_data['touchscreen'] = {
            'touchscreen_id': -1,
            'touchscreen_name': 'touchscreen not searched',
            'calibration': False,
            'axes_swap': False,
            'axis_inversion': False,
        }

        """
        self.xorgconf_data = {
            'displays': [
                {
                    'screen_num': 0,
                    'screen_id': 'screen-zero',
                    'monitor_id': '0-LVDS1',
                    'monitor_mode': '1280x768_60.00',
                    'monitor_modeline': 'blabla',
                    'monitor_x': 0,
                    'monitor_y': 0,
                    'monitor_w': 1280,
                    'monitor_h': 768,
                    'monitor_rotation': 'normal',
                    'monitor_output': 'LVDS1',
                },
                {
                    'screen_num': 1,
                    'screen_id': 'screen-one',
                    'monitor_id': '1-VGA1',
                    'monitor_mode': '1280x768_60.00',
                    'monitor_modeline': 'blabla',
                    'monitor_x': 0,
                    'monitor_y': 768,
                    'monitor_w': 1280,
                    'monitor_h': 768,
                    'monitor_rotation': 'normal',
                    'monitor_output': 'VGA1',
                },
            ],
            'touchcap': {
                'map-to-output': 'LVDS1',
                'geo_1x': 0,
                'geo_1y': 0,
                'geo_2x': 0,
                'geo_2y': 0,
                'geo_3x': 0,
                'geo_3y': 0,
                'geo_4x': 0,
                'geo_4y': 0,
            }
        }
        """


    def __get_modeline(self, width, height):
        call = subprocess.Popen(['cvt', str(width), str(height), '60'],
                                stdin=None, stdout=subprocess.PIPE)
        stdout, stderr = call.communicate()
        return stdout.split('\n')[1].split('"')[2].strip()

    def __compute_geometries(self, nb_screens, resolution, stack):
        assert(nb_screens > 0)
        assert('x' in resolution and len(resolution.split('x')) == 2)
        assert(stack == 'no' or stack.lower()[0] in ('v', 'h'))
        width, height = map(int, resolution.split('x'))
        left, top = 0, 0
        yield Geometry("{}x{}+{}+{}".format(width, height, left, top))
        if self.profile['multi_mode'] in ('normal', 'clone'):
            for i in range(nb_screens - 1):
                yield Geometry("{}x{}+{}+{}".format(width, height, left, top))
        else:
            for i in range(nb_screens - 1):
                if stack.lower().startswith('v'):
                    top += height
                elif stack.lower().startswith('h'):
                    left += width
                yield Geometry("{}x{}+{}+{}".format(width, height, left, top))
                #yield Geometry(width, height, left, top)

    ###################### Storing/Loading conf #####################

    staging_confdir = os.path.join(ATAWA_BASEDIR, 'conf/xsession_staging')

    def store_config(self):
        try:
            os.makedirs(self.staging_confdir)
        except OSError, e:
            if e.errno == 17: # File exists
                pass
            else:
                raise

        with open(os.path.join(self.staging_confdir, 'xorgconf_data.json'), 'w') as f:
            f.write(json.dumps(self.xorgconf_data))

        #with open(os.path.join(self.staging_confdir, 'profile.json'), 'w') as f:
        #    f.write(json.dumps(self.profile))

    def load_config(self):
        if os.path.exists(os.path.join(self.staging_confdir, 'xorgconf_data.json')):
            with open(os.path.join(self.staging_confdir, 'xorgconf_data.json'), 'r') as f:
                xorgconf_data = f.read()
            self.xorgconf_data = json.loads(xorgconf_data)


        #with open(os.path.join(self.staging_confdir, 'profile.json'), 'r') as f:
        #    self.profile = json.loads(f.read())


    ###################### Validating/Canceling config ######################

    xorgconf_bk = '{}/conf/X11/xorg.conf.bk'.format(ATAWA_BASEDIR)

    def validate_config(self):
        if self.config_schedcancel:
            self.config_schedcancel.cancel()
            self.config_schedcancel = None

        assert(os.path.exists('/etc/X11/xorg.conf'))
        if os.path.exists(self.xorgconf_bk):
            os.remove(self.xorgconf_bk)
        shutil.copy2('/etc/X11/xorg.conf', self.xorgconf_bk)

        self._define_firefox_target('home')
        #self._define_xsession_target('app')
        #self._restart_atawa_xsession()
        self._restart_full_atawa_xsession('app')

    def reset_config(self):
        if self.config_schedcancel:
            self.config_schedcancel.cancel()
            self.config_schedcancel = None

        if os.path.exists(self.staging_confdir):
            shutil.rmtree(self.staging_confdir)
        if os.path.exists('/etc/X11/xorg.conf'):
            os.remove('/etc/X11/xorg.conf')
        inputconf_file = '/usr/share/X11/xorg.conf.d/99-calibration.conf'
        if os.path.exists(inputconf_file):
            os.remove(inputconf_file)

        self._define_firefox_target('page_config')
        self._adjust_firefox_window(1280, 768)
        self._configure_app_panes_count(nb_panes=1)
        #self._define_xsession_target('app')
        #self._restart_atawa_xsession()
        self._restart_full_atawa_xsession('app')

    def cancel_config(self):
        if self.config_schedcancel:
            try:
                self.config_schedcancel.cancel()
            except:
                pass
            self.config_schedcancel = None
        if os.path.exists(self.staging_confdir):
            shutil.rmtree(self.staging_confdir)
        if os.path.exists('/etc/X11/xorg.conf'):
            os.remove('/etc/X11/xorg.conf')
        if os.path.exists(self.xorgconf_bk):
            shutil.copy2(self.xorgconf_bk, '/etc/X11/xorg.conf')

        self._define_firefox_target('page_config')
        self._adjust_firefox_window(1280, 768)
        #TODO: store (with a new method) the valid config and fetch/compute
        # the previous total virtual xsession size.

        #self._define_xsession_target('app')
        #self._restart_atawa_xsession()
        self._restart_full_atawa_xsession('app')

    def quit_config(self):
        self._define_firefox_target('home')

        with open('/tmp/quit_config_called', 'a') as f:
            f.write('touched by aas display config @ {}'.format(datetime.now()))


    firefox_target_file = '{}/conf/X11/firefox_target.url'.format(ATAWA_BASEDIR)

    def _define_firefox_target(self, firefox_target):
        registered_targets = {
            'home': '',
            'page_config':  'http://localhost/index2.php?configubuntu=config',
            'page_calib':  'http://localhost/index2.php?configubuntu=calibration',
            'page_confirm': 'http://localhost/index2.php?configubuntu=askconfirm',
        }

        assert(firefox_target in registered_targets)

        if firefox_target == 'home':
            if os.path.exists(self.firefox_target_file):
                os.remove(self.firefox_target_file)
        else:
            with open(self.firefox_target_file, 'w') as f:
                f.write(registered_targets[firefox_target])

    ######################### Applying conf #########################

    def apply_config(self):
        # use screenlayout to generate config with template of xorg.conf
        # in monitor section :
        #   - apply res
        #   - choose preferred mode
        #   -
        xorgconf_rendered = self._render_xorgconf()

        self._export_primary_monitor()

        xsession_w, xsession_h = self.__compute_xsession_size()
        self._adjust_firefox_window(xsession_w, xsession_h)

        ##TODO: apply to /etc/X11/70-atawa-xorg.conf
        with open('/etc/X11/xorg.conf', 'w') as f:
            f.write(xorgconf_rendered)

        self._configure_app_panes_count()

        self._define_firefox_target('page_confirm')
        self._restart_full_atawa_xsession('app')

        self.config_schedcancel = Timer(25.00, self.cancel_config)
        self.config_schedcancel.start()

        return xorgconf_rendered

    def _configure_app_panes_count(self, nb_panes=None):
        if nb_panes is None:
            nb_panes = len(self.xorgconf_data['active_displays']) if self._is_extend_mode() else 1
        assert (0 < nb_panes <= 4), "Le nombre d'écrans actifs est invalide : {}".format(nb_panes)

        multi_panes_dir = '/var/www/ChoixMultiEcrans'

        choice_dir_files = os.listdir(multi_panes_dir)

        for c in map(str, range(0, 10)):
            if c in choice_dir_files:
                os.remove(os.path.join(multi_panes_dir, c))

        with open(os.path.join(multi_panes_dir, str(nb_panes)), 'w') as f:
            f.write('created by aas display config @ {}'.format(datetime.now()))

    def _adjust_firefox_window(self, xsession_w, xsession_h):
        atawa_x11_confdir = os.path.join(ATAWA_BASEDIR, 'conf/X11')
        with open(os.path.join(atawa_x11_confdir, 'xsession_width'), 'w') as f:
            f.write(str(xsession_w))
        with open(os.path.join(atawa_x11_confdir, 'xsession_height'), 'w') as f:
            f.write(str(xsession_h))

    def _render_xorgconf(self):
        xorgconf_tpl_path = os.path.join(self.server_basedir,
                                         'conf.d/displays.conf.mustache')
        with open(xorgconf_tpl_path, 'r') as f:
            xorgconf_tpl_file = f.read().decode('utf-8')
        xorgconf_tpl_parsed = pystache.parse(xorgconf_tpl_file)
        xorgconf_rendered = pystache.render(xorgconf_tpl_parsed,
                                            self.xorgconf_data)
        return xorgconf_rendered

    def _export_primary_monitor(self):
        with open('{}/conf/X11/last_primary_monitor'.format(ATAWA_BASEDIR), 'w') as f:
            f.write(self.xorgconf_data['active_displays'][0]['monitor_output'])


    ######################### XSession handling #############################

    def _restart_full_atawa_xsession(self, xsession_target, clear_xorg=False, wait=5):
        self._define_xsession_target(xsession_target)
        if clear_xorg and os.path.exists('/etc/X11/xorg.conf'):
            os.remove('/etc/X11/xorg.conf')
        self._restart_atawa_xsession(wait=wait)


    def _restart_atawa_xsession(self, wait=0):
        rest_time = float(wait)
        assert (wait >= 0)
        """
        call = subprocess.Popen(['initctl', 'status', 'atawa-xsession'],
                                stdin=None,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE)
        stdout, stderr = call.communicate()

        if 'start' in stdout:

            if wait > 0:
                stdout, stderr = '', ''
                call = subprocess.Popen(['initctl', 'stop', 'atawa-xsession'],
                                        stdin=None,
                                        stdout=subprocess.PIPE,
                                        stderr=subprocess.PIPE)
                stdout_, stderr_ = call.communicate()
                stdout += str(stdout_)
                stderr += str(stderr_)

                time.sleep(rest_time)

                call = subprocess.Popen(['initctl', 'start', 'atawa-xsession'],
                                        stdin=None, stdout=subprocess.PIPE)
                stdout_, stderr_ = call.communicate()
                stdout += str(stdout_)
                stderr += str(stderr_)
            else:
                call = subprocess.Popen(['initctl', 'restart', 'atawa-xsession'],
                                        stdin=None, stdout=subprocess.PIPE)
                stdout, stderr = call.communicate()
        else:
            if wait > 0:
                time.sleep(wait)
            call = subprocess.Popen(['initctl', 'start', 'atawa-xsession'],
                                    stdin=None,
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE)
            stdout, stderr = call.communicate()
        """

	stdout, stderr = '', ''
	call = subprocess.Popen(['initctl', 'stop', 'atawa-xsession'],
				stdin=None,
				stdout=subprocess.PIPE,
				stderr=subprocess.PIPE)
	stdout_, stderr_ = call.communicate()
	stdout += str(stdout_)
	stderr += str(stderr_)

	time.sleep(rest_time)

	call = subprocess.Popen(['initctl', 'start', 'atawa-xsession'],
				stdin=None,
				stdout=subprocess.PIPE,
				stderr=subprocess.PIPE)
	stdout_, stderr_ = call.communicate()
	stdout += str(stdout_)
	stderr += str(stderr_)

        logger.info('Restarted atawa-xsession:\n'
                    '========================\n'
                    'stdout:{}\n'
                    'stderr:{}\n'
                    '========================'.format(stdout, stderr))
        return stdout, stderr

    """
    def _get_all_xsessions(self):
        return map(lambda f: f.split('.conf')[0], filter(lambda f: f.startswith('atawa') and 'xsession' in f, os.listdir('/etc/init')))

    def __ctl_xsession(self, action, xsession):
        assert action in ['start', 'restart', 'stop', 'status']
        assert xsession in map(lambda f: f.split('.conf')[0], filter(lambda f: f.startswith('atawa') and f.endswith('xsession.conf'), os.listdir('/etc/init')))
        call = subprocess.Popen(['initctl', action, xsession],
                                stdin=None, stdout=subprocess.PIPE)
        stdout, stderr = call.communicate()
        if stderr:
            print stderr
            raise BadStateXsessionError()
        out = stdout.replace(',', '').split()
        assert xsession == out[0], 'Mauvaise sortie de "initctl {} {}" : out:[{}] // err:[{}]'.format(
            action, xsession, stdout, stderr)

    def _reach_xsession(self, xsession):
        pass


    #def _toggle_xsession(self):

        appx_stdout, appx_stderr = self.__ctl_xsession('status', 'atawa-app-xsession')
        cfgx_stdout, cfgx_stderr = self.__ctl_xsession('status', 'atawa-waitconfig-xsession')

        if 'stop' in appx_stdout and 'stop' in cfgx_stdout:
            appx_stdout, appx_stderr = self.__ctl_xsession('start', 'atawa-app-xsession')
        elif 'start' in appx_stdout:
            appx_stdout, appx_stderr = self.__ctl_xsession('stop', 'atawa-app-xsession')
            cfgx_stdout, cfgx_stderr = self.__ctl_xsession('start', 'atawa-waitconfig-xsession')
        elif 'start' in cfgx_stdout:
            cfgx_stdout, cfgx_stderr = self.__ctl_xsession('stop', 'atawa-waitconfig-xsession')
            appx_stdout, appx_stderr = self.__ctl_xsession('start', 'atawa-app-xsession')
        else:
            raise BadStateXsessionError()
    """

    def _get_all_xsessions(self):
        return filter(lambda f: f.startswith('atawa') and f.endswith('xsession'), os.listdir('{}/bin'.format(ATAWA_BASEDIR)))

    def _define_xsession_target(self, xsession_target='app'):
        ax_link = '{}/bin/atawa-xsession'.format(ATAWA_BASEDIR)
        xsession_targets = {
            'app':              '{}/bin/atawa-app-xsession'.format(ATAWA_BASEDIR),
            'waitconfig':       '{}/bin/atawa-waitconfig-xsession'.format(ATAWA_BASEDIR),
        }

        #xsessions_path = [app_xsession_file, config_xsession_file]
        #xsessions_name = map(os.path.basename, [app_xsession_file, config_xsession_file])

        assert (xsession_target in xsession_targets)

        for xsession in xsession_targets.values():
            assert (os.path.exists(xsession))
            assert (os.path.basename(xsession) in os.listdir('{}/bin/'.format(ATAWA_BASEDIR)))

        """
        try:
            current_target = os.readlink(ax_link)
        except OSError as err:
            # [Errno 2] No such file or directory
            if err.errno == 2:
                current_target = config_xsession_file
            # [Errno 22] Invalid argument
            elif err.errno == 22 and os.path.exists(ax_link):
                os.unlink(ax_link)
                current_target = config_xsession_file
            else:
                raise

        assert(os.path.basename(current_target) in xsessions_name)

        # New Xsession selection
        xsessions_name.remove(os.path.basename(current_target))
        new_target = xsessions_name[0]
        """

        try:
            os.unlink(ax_link)
        except OSError as err:
            # [Errno 2] No such file or directory
            if err.errno == 2:
                pass
            else:
                raise

        os.symlink(xsession_targets[xsession_target], ax_link)

    def _is_extend_mode(self):
        allpos = set()
        for d in self.xorgconf_data['active_displays']:
            allpos.add('{}x{}'.format(d['monitor_x'], d['monitor_y']))
        return (len(allpos) > 1)

    def __compute_xsession_size(self):
        xsession_w = self.xorgconf_data['active_displays'][0]['monitor_w']
        xsession_h = self.xorgconf_data['active_displays'][0]['monitor_h']
        # Attention : ce calcul de la résolution virtuelle
        # de firefox ne peut fonctionner qu'à la condition que les
        # écrans soient tous empilés sur un même axe (vertical
        # ou horizontal) sans se chevaucher :
        """
        for display in self.xorgconf_data['displays'][1:]:
            if display['monitor_x'] == 0:
            xsession_w += display['monitor_w']
            xsession_h += display['monitor_h']
        """

        # S'il existe plusieurs écrans
        if len(self.xorgconf_data['active_displays']) > 1 and self._is_extend_mode():
            # soit on empile sur l'axe vertical
            if self.xorgconf_data['active_displays'][1]['monitor_x'] == 0:
                xsession_h = xsession_h * len(self.xorgconf_data['active_displays'])
            # soit on empile sur l'axe horizontal
            elif self.xorgconf_data['active_displays'][1]['monitor_y'] == 0:
                xsession_w = xsession_w * len(self.xorgconf_data['active_displays'])
            # sinon c'est une erreur de configuration
            else:
                raise VirtualLayoutError("Mauvais agencement des écrans")

        if self.xorgconf_data:
            rotation = self.xorgconf_data['active_displays'][0]['monitor_rotation']
            if rotation != 'normal':
                assert (rotation in ('right', 'left'))
                xsession_w, xsession_h = xsession_h, xsession_w

        return xsession_w, xsession_h

    ################### TouchScreen Config #######################

    def touchscreen_attach_output(self):
        self.load_config()
        touchscreen_id = self._detect_touchscreen()
        ret = 'attached touchscreen'
        try:
            self._attach_touchscreen(touchscreen_id)
        except Exception as err:
            ret = 'not {}: {}'.format(ret, err)
        else:
            self._update_inputconf()
            self.store_config()
        return ret

    def touchscreen_toggle_direction_x(self):
        self.load_config()
        touchscreen_id = self._detect_touchscreen()
        ret = 'toggled touchscreen axis x'
        try:
            self._toggle_direction_axis(touchscreen_id, 'x')
        except Exception as err:
            ret = 'not {}: {}'.format(ret, err)
        else:
            self._update_inputconf()
            self.store_config()
            self._define_firefox_target('page_calib')
            self._restart_full_atawa_xsession('app')
        return ret

    def touchscreen_toggle_direction_y(self):
        self.load_config()
        touchscreen_id = self._detect_touchscreen()
        ret = 'toggled touchscreen axis y'
        try:
            self._toggle_direction_axis(touchscreen_id, 'y')
        except Exception as err:
            ret = 'not {}: {}'.format(ret, err)
        else:
            self._update_inputconf()
            self.store_config()
            self._define_firefox_target('page_calib')
            self._restart_full_atawa_xsession('app')
        return ret

    def touchscreen_toggle_swap_axes_xy(self):
        self.load_config()
        touchscreen_id = self._detect_touchscreen()
        ret = 'toggled swap axes x/y'
        try:
            self._toggle_swap_axes_xy(touchscreen_id)
        except Exception as err:
            ret = 'not {}: {}'.format(ret, err)
        else:
            self._update_inputconf()
            self.store_config()
            self._define_firefox_target('page_calib')
            self._restart_full_atawa_xsession('app')
        return ret

    def touchscreen_get_config(self):
        self.load_config()
        try:
            touchscreen_id = self._detect_touchscreen()
            ret = self._xinput_get_properties(touchscreen_id)
        except:
            ret = 'Error getting config for touchscreen'
        else:
            self._update_inputconf()
            self.store_config()
        return ret

    def touchscreen_calibrate(self):
        self.load_config()
        touchscreen_id = self._detect_touchscreen()
        ret = 'touchscreen calibrated'
        try:
            self._attach_touchscreen(touchscreen_id)
            self._xinput_calibrate(touchscreen_id)
        except Exception as err:
            ret = 'not {}: {}'.format(ret, err)
        else:
            self._update_inputconf()
            self.store_config()
            self._define_firefox_target('page_calib')
            self._restart_full_atawa_xsession('app')
        return ret

    def _render_inputconf(self):
        inputconf_tpl_path = os.path.join(self.server_basedir,
                                         'conf.d/touchscreen.conf.mustache')
        with open(inputconf_tpl_path, 'r') as f:
            inputconf_tpl_file = f.read().decode('utf-8')
        inputconf_tpl_parsed = pystache.parse(inputconf_tpl_file)
        inputconf_rendered = pystache.render(inputconf_tpl_parsed,
                                            self.xorgconf_data['touchscreen'])
        return inputconf_rendered

    def _update_inputconf(self):
        inputconf_rendered = self._render_inputconf()
        inputconf_file = '/usr/share/X11/xorg.conf.d/99-calibration.conf'
        with open(inputconf_file, 'w') as f:
            f.write(inputconf_rendered)
        os.chmod(inputconf_file, 0664)

    def _xinput_map_output(self, touchscreen_id, output_name):
        assert (touchscreen_id in self._xinput_get_devices().values()), \
            "Touchscreen id invalid: {}".format(touchscreen_id)
        call = subprocess.Popen(['su -c "DISPLAY=:0.0 xinput map-to-output {} {}" atawa'.\
                                    format(touchscreen_id, output_name)],
                                stdin=None,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE,
                                shell=True
                                )
        stdout, stderr = call.communicate()

    def _attach_touchscreen(self, touchscreen_id):
        if not self.xorgconf_data:
            self.load_config()
        output_name = self.xorgconf_data['active_displays'][0]['monitor_output']
        self._xinput_map_output(touchscreen_id, output_name)

    def _xinput_calibrate(self, touchscreen_id):
        assert (touchscreen_id in self._xinput_get_devices().values()), \
            "Touchscreen id invalid: {}".format(touchscreen_id)
        calib_tmp_ = tempfile.mkstemp('_ts_calib')
        calib_tmp = calib_tmp_[1]
        os.chmod(calib_tmp, 0666)
        #xorgconf_tscalib = '/usr/share/X11/xorg.conf.d/99-calibration.conf'
        call = subprocess.Popen(['su -c "DISPLAY=:0.0 xinput_calibrator --device {} --output-filename {}" atawa'.\
                                 format(touchscreen_id, calib_tmp)],
                                 stdin=None,
                                 stdout=subprocess.PIPE,
                                 stderr=subprocess.PIPE,
                                 shell=True
                                 )
        stdout, stderr = call.communicate()
        with open(calib_tmp, 'r') as f:
            touchscreen_calib = f.read()
        xc_params = re.findall('\tOption\t\"Calibration\"\t\"([\d\- ]+)\"\n\tOption\t\"SwapAxes\"\t\"([\d]+)\"\nEndSection$', touchscreen_calib)
        try:
            xc_calib = xc_params[0][0]
            xc_swap_axes = xc_params[0][1]
        except IndexError as err:
            raise("{}\nErreur de parsage du fichier créé par xinput_calibrator: {}".format(err, xc_params))
        self.xorgconf_data['touchscreen']['calibration'] = [{'calib_coord': xc_calib}]
        #self._toggle_swap_axes_xy(touchscreen_id, to_value=bool(int(xc_swap_axes)))

    def _xinput_set_property(self, touchscreen_id, xinput_property, property_value):
        assert (touchscreen_id in self._xinput_get_devices().values()), \
            "Touchscreen id invalid: {}".format(touchscreen_id)
        xinput_cmd = 'su -c "DISPLAY=:0.0 xinput set-prop {} \'{}\' {}" atawa'.\
                                 format(touchscreen_id, xinput_property, property_value)
        call = subprocess.Popen([xinput_cmd],
                                 stdin=None,
                                 stdout=subprocess.PIPE,
                                 stderr=subprocess.PIPE,
                                 shell=True
                                 )
        stdout, stderr = call.communicate()
        return stdout

    def _xinput_get_properties(self, touchscreen_id):
        assert (touchscreen_id in self._xinput_get_devices().values()), \
            "Touchscreen id invalid: {}".format(touchscreen_id)
        call = subprocess.Popen(['su -c "DISPLAY=:0.0 xinput list-props {}" atawa'.\
                                 format(touchscreen_id)],
                                 stdin=None,
                                 stdout=subprocess.PIPE,
                                 stderr=subprocess.PIPE,
                                 shell=True
                                 )
        stdout, stderr = call.communicate()
        if stderr:
            raise Exception("Error: calling xinput for properties of device {}:\n{}".\
                            format(touchscreen_id, stderr))
        else:
            return stdout

    def _xinput_get_property(self, touchscreen_id, xinput_property):
        properties_output = self._xinput_get_properties(touchscreen_id)
        props = map(lambda l: {l.split(':')[0].strip(): ''.join(map(str.strip, l.split(':')[1:]))},
                    properties_output.split('\n'))
        props = {k: v for p in props for k, v in p.items()}
        for p, v in props.items():
            if xinput_property in p:
                return v
        raise Exception("Error: xinput property not found '{}' for device {}".\
                        format(xinput_property, touchscreen_id))

    def _toggle_direction_axis(self, touchscreen_id, axis):
        axes = {'x': 0, 'y': 1}
        assert (axis in axes), "Axis not valid: {}".format(axis)
        current_value = self._xinput_get_property(touchscreen_id, 'Evdev Axis Inversion')
        axes_inv = map(lambda x: bool(int(x.strip())), current_value.split(','))
        axes_inv[axes[axis]] = not axes_inv[axes[axis]]
        trans_value = lambda a: str({True: 1, False: 0}[a])
        new_values = ', '.join(map(trans_value, axes_inv))
        inverse_x = trans_value(axes_inv[0])
        inverse_y = trans_value(axes_inv[1])
        #self._xinput_set_property(touchscreen_id, 'Evdev Axis Inversion', new_values)
        self.xorgconf_data['touchscreen']['axis_inversion'] = [{
            'inverse_x': inverse_x,
            'inverse_y': inverse_y,
        }]

    def _toggle_swap_axes_xy(self, touchscreen_id, to_value=None):
        if to_value is None:
            current_value = self._xinput_get_property(touchscreen_id, 'Evdev Axes Swap')
            trans_value = bool(int(current_value))
            values_trans = {True: 1, False: 0}
            swap_xy = '{}'.format(values_trans[not trans_value])
        else:
            assert(to_value in (True, False))
            values_trans = {True: 1, False: 0}
            swap_xy = '{}'.format(values_trans[bool(int(to_value))])
        #self._xinput_set_property(touchscreen_id, 'Evdev Axes Swap', swap_xy)
        self.xorgconf_data['touchscreen']['axes_swap'] = [{'swap_xy': swap_xy}]

    def _detect_touchscreen(self):
        dev_id, dev_name = self._detect_touchscreen_id()
        with open('{}/conf/X11/last_touchscreen_id'.format(ATAWA_BASEDIR), 'w') as f:
            f.write('{}'.format(dev_id))
        self.xorgconf_data['touchscreen']['touchscreen_id'] = dev_id
        self.xorgconf_data['touchscreen']['touchscreen_name'] = dev_name
        return dev_id

    def _xinput_get_devices(self):
        call = subprocess.Popen(['su -c "DISPLAY=:0.0 xinput" atawa'],
                                 stdin=None,
                                 stdout=subprocess.PIPE,
                                 stderr=subprocess.PIPE,
                                 shell=True
                                 )
        stdout, stderr = call.communicate()
        all_dev_id = re.findall('↳ ([\S ]+)\tid=(\d+)', stdout)
        xinput_devices = {}
        for dev_info in all_dev_id:
            xinput_devices[dev_info[0].strip()] = int(dev_info[1])
        return xinput_devices

    def _detect_touchscreen_id(self):

        xinput_devices = self._xinput_get_devices()

        expected_patterns = [
            'zaag',
            'elo touch', 'elotouch',
        ]

        #TODO:change to a cfg file :
        #       - devname in section
        #       - patterns
        #       - need calibration with xinput_calibrator ?
        #       - launch special script ?
        #       - driver to launch before with lsusb ?


        for dev_name, dev_id in xinput_devices.items():
            for dev_pattern in expected_patterns:
                if dev_pattern.lower() in dev_name.lower():
                    return (dev_id, dev_name)
        else:
            return (-1, 'touchscreen not found')


    ################## Showing Config ####################

    def show_config(self):
        out = ''
        if self.profile:
            out += '============= Atawa Xsession Automatic profile ==============\n'
            out += json.dumps(self.profile,
                              sort_keys=True,
                              indent=4,
                              separators=(',', ': '))
        else:
            out += '============= Atawa Xsession Manual profile ==============\n'
            out += 'no profile\n'
        out += '\n'

        out += '============= Data for xorg.conf ==============\n'
        if self.xorgconf_data:
            out += json.dumps(self.xorgconf_data,
                              #sort_keys=True,
                              indent=4,
                              separators=(',', ': '))
        else:
            out += 'no data\n'
        out += '\n'

        out += '========== XRandR Output (before the new conf) =============\n'
        out += self.displays_config._xrandr._output()

        return out
