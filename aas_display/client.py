# -*- encoding: utf-8 -*-

import sys
import click

from twisted.web.xmlrpc import Proxy
from twisted.internet import reactor

import logging


def printValue(value):
    print value
    reactor.stop()


def printError(error):
    print >>sys.stdout, 'error: {}'.format(error)
    reactor.stop()


@click.group()
@click.option('-v', '--verbose', is_flag=True)
def cli(verbose):
    loglevel = logging.INFO if verbose else logging.ERROR
    logging.basicConfig(level=loglevel)


@cli.command()
def get_options_auto():
    logging.info('Display options for automatic configuration')
    proxy = Proxy('http://127.0.0.1:7080/XMLRPC')
    proxy.callRemote('display.get_options_auto').\
        addCallbacks(printValue, printError)
    reactor.run()


@cli.command()
def get_options_manual():
    logging.info('Display options for manual configuration')
    proxy = Proxy('http://127.0.0.1:7080/XMLRPC')
    proxy.callRemote('display.get_options_manual').\
        addCallbacks(printValue, printError)
    reactor.run()


@cli.command()
def get_config():
    logging.info('Get configuration')
    proxy = Proxy('http://127.0.0.1:7080/XMLRPC')
    proxy.callRemote('display.get_config').\
        addCallbacks(printValue, printError)
    reactor.run()


@cli.command()
@click.argument('product', type=str)
@click.argument('hardware', type=str)
@click.argument('nbscreens', type=int)
@click.argument('multimode', type=str)
def config_auto(product, hardware, nbscreens, multimode):
    logging.info('Start automatic configuration')
    proxy = Proxy('http://127.0.0.1:7080/XMLRPC')
    proxy.callRemote(
        'display.config_auto',
        product, hardware, nbscreens, multimode).\
        addCallbacks(printValue, printError)
    reactor.run()


@cli.command()
def config_manual():
    logging.info('Start manual configuration')
    proxy = Proxy('http://127.0.0.1:7080/XMLRPC')
    proxy.callRemote('display.config_manual').\
        addCallbacks(printValue, printError)
    reactor.run()


@cli.command()
def validate_config():
    logging.info('Validate configuration')
    proxy = Proxy('http://127.0.0.1:7080/XMLRPC')
    proxy.callRemote('display.validate_config').\
        addCallbacks(printValue, printError)
    reactor.run()


@cli.command()
def cancel_config():
    logging.info('Cancel configuration')
    proxy = Proxy('http://127.0.0.1:7080/XMLRPC')
    proxy.callRemote('display.cancel_config').\
        addCallbacks(printValue, printError)
    reactor.run()


@cli.command()
def reset_config():
    logging.info('Reset configuration')
    proxy = Proxy('http://127.0.0.1:7080/XMLRPC')
    proxy.callRemote('display.reset_config').\
        addCallbacks(printValue, printError)
    reactor.run()


@cli.command()
def quit_config():
    logging.info('Reset configuration')
    proxy = Proxy('http://127.0.0.1:7080/XMLRPC')
    proxy.callRemote('display.quit_config').\
        addCallbacks(printValue, printError)
    reactor.run()


@cli.command()
def touchscreen_attach_output():
    logging.info('Reset configuration')
    proxy = Proxy('http://127.0.0.1:7080/XMLRPC')
    proxy.callRemote('display.touchscreen_attach_output').\
        addCallbacks(printValue, printError)
    reactor.run()


@cli.command()
def touchscreen_toggle_direction_axis_x():
    logging.info('Reset configuration')
    proxy = Proxy('http://127.0.0.1:7080/XMLRPC')
    proxy.callRemote('display.touchscreen_toggle_direction_axis_x').\
        addCallbacks(printValue, printError)
    reactor.run()


@cli.command()
def touchscreen_toggle_direction_axis_y():
    logging.info('Reset configuration')
    proxy = Proxy('http://127.0.0.1:7080/XMLRPC')
    proxy.callRemote('display.touchscreen_toggle_direction_axis_y').\
        addCallbacks(printValue, printError)
    reactor.run()


@cli.command()
def touchscreen_toggle_swap_axes_xy():
    logging.info('Reset configuration')
    proxy = Proxy('http://127.0.0.1:7080/XMLRPC')
    proxy.callRemote('display.touchscreen_toggle_swap_axes_xy').\
        addCallbacks(printValue, printError)
    reactor.run()


@cli.command()
def touchscreen_get_config():
    logging.info('Reset configuration')
    proxy = Proxy('http://127.0.0.1:7080/XMLRPC')
    proxy.callRemote('display.touchscreen_get_config').\
        addCallbacks(printValue, printError)
    reactor.run()


@cli.command()
def touchscreen_calibrate():
    logging.info('Reset configuration')
    proxy = Proxy('http://127.0.0.1:7080/XMLRPC')
    proxy.callRemote('display.touchscreen_calibrate').\
        addCallbacks(printValue, printError)
    reactor.run()

if __name__ == '__main__':
    cli()
